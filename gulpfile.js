'use strict';
const gulp = require('gulp');
const debug = require('gulp-debug');
const del = require('del');
const eventStream = require('event-stream');

const paths = {
    root: './',
    nodeModules: './node_modules/',
    dist: {
        dir: './dist/',
        css: './dist/css/',
        js: './dist/js/'
    }
};

const tasks = {
    default: 'default',
    vendor: {
        name: 'vendor',
        copy: 'vendor-copy',
        clear: 'vendor-clear'
    }
};

gulp.task(tasks.default, [tasks.vendor.name]);

gulp.task(tasks.vendor.name, [tasks.vendor.clear, tasks.vendor.copy]);

gulp.task(tasks.vendor.copy, () => {
    return eventStream.merge([
        gulp.src([
            `${paths.nodeModules}bootstrap/dist/css/bootstrap.css`,
            `${paths.nodeModules}bootstrap/dist/css/bootstrap.min.css`
        ])
        .pipe(debug({title: 'bootstrap css'}))
        .pipe(gulp.dest(paths.dist.css)),

        gulp.src([
            `${paths.nodeModules}bootstrap/dist/js/bootstrap.js`,
            `${paths.nodeModules}bootstrap/dist/js/bootstrap.min.js`
        ])
        .pipe(debug({title: 'bootstrap js'}))
        .pipe(gulp.dest(paths.dist.js)),

        gulp.src([
                `${paths.nodeModules}jquery/dist/jquery.slim.js`,
                `${paths.nodeModules}jquery/dist/jquery.slim.min.js`
            ])
            .pipe(debug({title: 'jquery'}))
            .pipe(gulp.dest(paths.dist.js)),

        gulp.src([
                `${paths.nodeModules}popper.js/dist/umd/popper.js`,
                `${paths.nodeModules}popper.js/dist/umd/popper.min.js`
            ])
            .pipe(debug({title: 'popper'}))
            .pipe(gulp.dest(paths.dist.js))
    ]);
});

gulp.task(tasks.vendor.clear, () => {
    return del.sync([
        `${paths.dist.css}**.*`,
        `!${paths.dist.css}`,
        `${paths.dist.js}**.*`,
        `!${paths.dist.js}`
    ]);
})