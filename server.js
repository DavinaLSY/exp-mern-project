//#region Variables
const express = require('express');
const path = require('path');
const mongoose = require('mongoose');

const pathsConfig = require('./src/config/paths.config');
const databaseConfig = require('./src/config/database.config');

const errorsLib = require('./src/lib/errors.lib');
//#endregion


//#region App
const app = express();

app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

app.use(express.static(pathsConfig.dist));

app.use(pathsConfig.dist, express.static(path.join(__dirname, pathsConfig.dist)));

app.get(pathsConfig.root, (request, response) => {
   response.json({
       message: "Welcome to the API of my MERN experimental project!"
   }); 
});

app.listen(databaseConfig.port, () => {
    console.log(`Server started on port ${databaseConfig.port}.`);
});
//#endregion


//#region Database
mongoose.Promise = global.Promise;

mongoose.connect(databaseConfig.url, { useNewUrlParser: true })
    .then(() => {
        console.log(`Connected to "${databaseConfig.url}".`);
    })
    .catch(err => {
        console.log(`Failed to connect to "${databaseConfig.url}".`);
        errorsLib.databaseError('database', err).print();
        process.exit();
    });
//#endregion