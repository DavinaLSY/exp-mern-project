module.exports = {
    root: "./",
    nodeModules: "./node_modules/",
    dist: "./dist/",
    src: "./src/",
    config: "./src/config"
};