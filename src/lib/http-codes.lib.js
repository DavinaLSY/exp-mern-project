module.exports = {
    //commonly used
    'Success2XX': {
        'success':                          '2XX',  '2XX': '2XX',
        'ok':                               200,    '200': 200,
        'created':                          201,    '201': 201,
        'noContent':                        204,    '204': 204
    },

    'Redirection3XX': {
        'redirection':                      '3XX',  '3XX': '3XX',
        'found':                            302,    '302': 302
    },

    'ClientError4XX': {
        'clientError':                      '4XX',  '4XX': '4XX',
        'badRequest':                       400,    '400': 400,
        'unauthorized':                     401,    '401': 401,
        'paymentRequired':                  402,    '402': 402,
        'forbidden':                        403,    '403': 403,
        'notFound':                         404,    '404': 404,
        'conflict':                         409,    '409': 409
    },

    'ServerError5XX': {
        'serverError':                      '5XX',  '5XX': '5XX',
        'internalServerError':              500,    '500': 500
    },

    'All': {
        'Informational1XX': {
            'informational':                    '1XX',  '1XX': '1XX',
            'continue':                         100,    '100': 100,
            'switchingProtocols':               101,    '101': 101,
            'earlyHints':                       103,    '103': 103
        },
        

        'Success2XX': {
            'success':                          '2XX',  '2XX': '2XX',
            'ok':                               200,    '200': 200,
            'created':                          201,    '201': 201,
            'accepted':                         202,    '202': 202,
            'nonAuthoritativeInformation':      203,    '203': 203,
            'noContent':                        204,    '204': 204,
            'resetContent':                     205,    '205': 205,
            'partialContent':                   206,    '206': 206,
            'imUsed':                           226,    '226': 226,
        },

        'Redirection3XX': {
            'redirection':                      '3XX',  '3XX': '3XX',
            'multipleChoices':                  300,    '300': 300,
            'movedPermanently':                 301,    '301': 301,
            'found':                            302,    '302': 302,
            'seeOther':                         303,    '303': 303,
            'notModified':                      304,    '304': 304,
            'useProxy':                         305,    '305': 305,
            'unused':                           306,    '306': 306,
            'temporaryRedirect':                307,    '307': 307
        },

        'ClientError4XX': {
            'clientError':                      '4XX',  '4XX': '4XX',
            'badRequest':                       400,    '400': 400,
            'unauthorized':                     401,    '401': 401,
            'paymentRequired':                  402,    '402': 402,
            'forbidden':                        403,    '403': 403,
            'notFound':                         404,    '404': 404,
            'methodNotAllowed':                 405,    '405': 405,
            'notAcceptable':                    406,    '406': 406,
            'proxyAuthenticationRequired':      407,    '407': 407,
            'requestTimeout':                   408,    '408': 408,
            'conflict':                         409,    '409': 409,
            'gone':                             410,    '410': 410,
            'lengthRequired':                   411,    '411': 411,
            'reconditionedFailed':              412,    '412': 412,
            'requestEntityTooLarge':            413,    '413': 413,
            'requestUriTooLong':                414,    '414': 414,
            'unsupportedMediaType':             415,    '415': 415,
            'requestedRangeNotSatisfiable':     416,    '416': 416,
            'expectationFailed':                417,    '417': 417,
            'upgradeRequired':                  426,    '426': 426,
            'reconditionRequired':              428,    '428': 428,
            'tooManyRequests':                  429,    '429': 429,
            'requestHeaderFieldsTooLarge':      431,    '431': 431,
            'unavailableForLegalReasons':       451,    '451': 451
        },

        'ServerError5XX': {
            'serverError':                      '5XX',  '5XX': '5XX',
            'internalServerError':              500,    '500': 500,
            'notImplemented':                   501,    '501': 501,
            'badGateway':                       502,    '502': 502,
            'serviceUnavailable':               503,    '503': 503,
            'gatewayTimeout':                   504,    '504': 504,
            'httpVersionNotSupported':          505,    '505': 505,
            'notExtended':                      510,    '510': 510,
            'networkAuthenticationRequired':    511,    '511': 511,
            'networkReadTimeoutError':          598,    '598': 598,
            'networkConnectTimeoutError':       599,    '599': 599
        },
    },

    'WebDAV': {
        'Informational1XX': {
            'informational':                        '1XX',  '1XX': '1XX',
            'processing':                           102,    '102': 102
        },
        
    
        'Success2XX': {
            'success':                              '2XX',  '2XX': '2XX',
            'multiStatus':                          207,    '207': 207,
            'alreadyReported':                      208,    '208': 208
        },
    
        'ClientError4XX': {
            'clientError':                          '4XX',  '4XX': '4XX',
            'unprocessableEntity':                  422,    '422': 422,
            'locked':                               423,    '423': 423,
            'failedDependency':                     424,    '424': 424,
            'reservedFor':                          425,    '425': 425
        },
    
        'ServerError5XX': {
            'serverError':                          '5XX',  '5XX': '5XX',
            'insufficientStorage':                  507,    '507': 507,
            'loopDetected':                         508,    '508': 508
        }
    },

    'Twitter': {
        'ClientError4XX': {
            'clientError':                          '4XX',  '4XX': '4XX',
            'enhanceYourCalm':                      420,    '420': 420
        },
    },

    'Nginx': {
        'ClientError4XX': {
            'clientError':                          '4XX',  '4XX': '4XX',
            'noResponse':                           444,    '444': 444,
            'clientClosedRequest':                  499,    '499': 499
        }
    },

    'Microsoft': {
        'ClientError4XX': {
            'retryWith':                            449,    '449': 449,
            'blockedByWindowsParentalControls':     450,   '450': 450
        }
    },

    'Apache': {
        'ServerError5XX': {
            'serverError':                          '5XX',  '5XX': '5XX',
            'bandwidthLimitExceeded':               509,    '509': 509,
        }
    },

    'experimental': {
        'Redirection3XX': {
            'redirection':                          '3XX',  '3XX': '3XX',
            'permanentRedirect':                    308,    '308': 308
        },
    
        'ServerError5XX': {
            'serverError':                          '5XX',  '5XX': '5XX',
            'variantAlsoNegotiates':                506,    '506': 506
        }
    },

    'rfc2324': {
        'ClientError4XX': {
            'clientError':                          '4XX',  '4XX': '4XX',
            'imaTeapot':                            418,    '418': 418
        }
    }
};