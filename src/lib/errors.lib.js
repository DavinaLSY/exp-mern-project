const Errors = {};

Errors.error = (field, message, type) => {
    const error = {
        type: type || 'Error',
        field: field,
        message: message || "An error has occurred."
    };

    error.string = () => {
        return `${error.type}\n\tfield: ${error.field}\n\tmessage: ${error.message}`
    };
    error.print = () => {
        console.log(error.toString);
    };

    return error;
}

Error.databaseError = (field, message) => {
    return Errors.error(field, message || "A database error has occurred.", 'DatabaseError');
}

module.exports = Errors;